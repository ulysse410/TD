## Exercices sur GIT et VIM

Tous les TD et TP de ce modules sont à faire sur une machine virtuelle. Vous pouvez récupérer une
debian minimal sur rt-cloud. Attention, vous n'avez pas besoin d'interface graphique dans votre machine
virtuelle. Votre système hôte vous le fournis.

### TD:

- [GIT](https://gitlab.com/m3206/TD/blob/master/GIT.md)
- [VIM](https://gitlab.com/m3206/TD/blob/master/VIM.md)
- [FIND](https://gitlab.com/m3206/TD/blob/master/FIND.md)


### Remarques

Si vous avez des remarques (fautes, questions, découvertes, lien intéressants, ... peu importe en fait tant
que c'est lié plus ou moins ou cours) 
concernant le cours, ouvrez une issue. Cet onglet ce trouve en haut de cette page.

Vous avez la listes des onglets: `Project/Activity/.../Issues`



### Quelques point à retenir

#### vim
Dans tous les cas je suppose qu'on est dans le mode édition, le mode par défaut de vim. 
Pour arriver dans ce mode il vous suffit d'appuyer sur `<esc>` peut importe le mode ou 
vous être.

- Write et Quit (sauvegarder et quitter): `:wq`
- Quit (quitter): `:q`
- les movements `h j k l`
- les verbes `delete: d`, `yank/copy: y`, `change/remplacer: c`, `paste/coller: p`, `open new line: o`, `open new line above: O`,
- les modificateurs: `inside: i`, `till: t`, `search: /`
- Les objets : `word: w`, `paragraph: p`, `sentence: s` ...
- doubler les verbes, l'applique sur une ligne: `dd`, `yy`
- Un nombre effectuer plusieus fois le verbe sur l'objet: `d3w
- exemple: Delete Inside Parenthesis: `di(` ou `di)` si le curseur est dans les parenthèses.
- Autres verbes: `insert: i`, `insert at the begining of line: I`, `add text: a`, `add text at the begining of line: A`

#### git
Je suppose que vous avez déjà fait un: `git clone https://....` de votre dépôt.
Le procesuss est le suivant.
- Vous modifiez un ou plusieurs fichiers, avec vim par exemple
- Pour chaque fichier (je dis bien pour chaque fichier) modifié vous faites: `git add fichier` 
- Ensuite, vous publiez votre travail en faisant: `git commit -m "Mettez ici votre message de commit"`
- Ensuite, vous envoyez le commit sur gitlab ou github: `git push origin master`

Si vous avez deja un clone de votre dépôt. Vous devez vous mettre dans votre dépôt git sur votre machine local.
Vous pouvez allez chercher la dernière version sur gitlab ou github avec la commande `git pull`. 

#### tmux

Vous lancer tmux dans un terminal: `tmux`
- Pour 'splitter' l'ecran verticalement: `<CTRL>+B` puis `%`
- Pour 'splitter' l'ecran horizontalement: `<CTRL>+B` puis `"`
- Pour changer de terminal : `<CTRL>+B` puis `fleche direction`
- `<CTRL>+D` pour fermer un terminal
- `<CTRL>+B` puis `d` pour detacher une session tmux 
- `tmux a` pour se rattacher à la dernière session