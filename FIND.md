## Quelques exemples sur find

__Vous pouvez conserver ce que vous faites dans votre dépôt en créant un
fichier readme.md, ou en mettant les commandes dans un fichier__

##### l'option `-exec` de `find`

Elle permet  d'executer une commande sur le résultat de `find`
```bash
$ find . -name *.gif -exec ls {} \;
```

##### l'option `-ok` de `find`

Vous devez créer plusieurs fichiers dont l'extenstion est `.txt` dans le dossier
`/tmp`. 

Que fait la commande suivante ? Que fait l'option `-ok` ? 
```bash
$ find /tmp -name *.txt -ok rm {} \;
```

##### Combiner `find` et `cpio`

Que fait la commande cpio ? Créez un script, ou trouvez la ligne de commande 
permettant de copier tous les fichers d'un répertoire. 

```bash
$ combiner find et cpio
```

##### `find` et l'option `-exec` (pour vous aider, combiner avec la commande `head`, ou `sed` ou `awk`)

Trouvez la commande qui permet de lister les premières lignes de tous les fichiers
*.txt d'un répertoire.

```bash
$ find ... -exec
```


## Quelques exemples de `awk`

#### Nombre champs de chaque ligne suivit de la ligne

La commande suivante ecrit chaque ligne suivi précédé du nombre de champs. `awk`
sait traiter les fichiers avec des nombres de champs différents.

```bash
awk '{ print NF ":" $0 } '
```

- Ecrivez en awk un script qui n'ecrit ni le premier ni le dernier champ de chaque ligne.
- Ecrivez le dernier champs de la dernière ligne d'un fichier avec awk (utiliser NF, et END)

## Quelques exemples de `sed`

[ici](http://www.catonmat.net/blog/sed-one-liners-explained-part-one/)
